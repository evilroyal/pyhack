"""
    Utility type-hints used throughout pyhack
"""
from typing import Tuple

Vec2i = Tuple[int, int]
Room = Tuple[int, int, int, int]
