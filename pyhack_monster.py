"""
    Provides a basic Monster class.
"""

from entity import Entity
from pyhack_state import GameState
from dungeon_level import is_walkable

def sign(int_val: int) -> int:
    """Returns the sign of the given integer."""
    if int_val == 0:
        return 0
    return -1 if int_val < 0 else 1

class Monster(Entity):
    """Most basic enemy type"""
    def __init__(self, x: int, y: int):
        super().__init__(x, y)
        self.position = (x, y)
        self.char_display = "x"
        self.current_hp = 5
        self.attack = 1

    def turn_update(self, game_state: GameState) -> bool:
        p_x, p_y = game_state.player.position
        c_x, c_y = self.position

        n_x, n_y = c_x + sign(p_x - c_x), c_y + sign(p_y - c_y)
        if is_walkable(game_state.current_level.get_cell(n_x, n_y)) and \
            not (n_x, n_y) == game_state.player.position:
            self.position = n_x, n_y
            return True

        return False

    def is_solid(self) -> bool:
        return True

    def is_hostile(self) -> bool:
        return True

    def get_char_display(self) -> str:
        return self.char_display

    def get_name(self) -> str:
        return "Monster"

    def attack_damage(self) -> int:
        return 2
