"""
    This module stores the player's information.
"""
from typing import Tuple, Set
from dungeon_level import DungeonLevel, is_walkable

import constants

Point = Tuple[int, int]


class Player:
    """
        Holds a player's state, which includes its position, HP, etc...
    """

    def __init__(self, position: Tuple[int, int]) -> None:
        self.current_hp = constants.INITIAL_HP
        self.max_hp = constants.INITIAL_HP
        self.position: Point = position
        self.tile_memory: Set[Point] = set()

        self.attack = constants.INITIAL_ATK

    def get_x_pos(self) -> int:
        """Returns the player's position's x coordinate"""
        return self.position[0]

    def get_y_pos(self) -> int:
        """Returns the player's position's y coordinate"""
        return self.position[1]

    def set_position(self, p_x: int, p_y: int):
        """Sets this player's position"""
        self.position = (p_x, p_y)

    # pylint: disable=R0912
    def compute_field_of_view(
            self,
            dungeon: DungeonLevel,
            viewport: Tuple[int, int, int, int] = None,
            save_to_memory: bool = True,
    ) -> Set[Point]:
        """
            Returns the set of the tiles that are visible by the player.
            Also saves the computed tiles to the player's "tile memory".
        """
        visible_tiles: Set[Point] = set()

        x_p, y_p = self.position

        # If viewport not specified, compute over whole dungeon level.
        if viewport is None:
            viewport = (0, 0, dungeon.get_width() - 1, dungeon.get_height() - 1)
        min_x, min_y, max_x, max_y = viewport

        # Ray-casting algorithm to find tiles in sight.
        for c_theta, s_theta in constants.VISION_COS_SIN:
            approx_iter: float = 0.0 # x position over line from the player.
            wall_or_out = False
            while not wall_or_out:
                x_d, y_d = (
                    round(x_p + approx_iter * c_theta),
                    round(y_p + approx_iter * s_theta),
                )

                if min_x <= x_d <= max_x and min_y <= y_d <= max_y:
                    if not is_walkable(dungeon.get_cell(x_d, y_d)):
                        wall_or_out = True
                    visible_tiles.add((x_d, y_d))
                else:
                    wall_or_out = True

                approx_iter += constants.VISION_RAY_SAMPLING_INTERVAL

        # If player in room, make the whole room is visble.
        player_room = dungeon.find_room(self.position)
        if player_room is not None:
            pr_x, pr_y, pr_w, pr_h = player_room
            for i_x in range(max(pr_x - 1, min_x), min(pr_x + pr_w + 1, max_x)):
                for i_y in range(max(pr_y - 1, min_y), min(pr_y + pr_h + 1, max_y)):
                    visible_tiles.add((i_x, i_y))

        # Make the walls in long corridors visible.
        p_x, p_y = self.position
        directions = [(1, 0), (-1, 0), (0, 1), (0, -1)]
        for inc_x, inc_y in directions:
            x_i, y_i = (p_x + inc_x, p_y + inc_y)
            while dungeon.is_valid_position(x_i, y_i) and is_walkable(dungeon.get_cell(x_i, y_i)):
                if not is_walkable(dungeon.get_cell(x_i + inc_y, y_i + inc_x)):
                    visible_tiles.add((x_i + inc_y, y_i + inc_x))
                if not is_walkable(dungeon.get_cell(x_i - inc_y, y_i - inc_x)):
                    visible_tiles.add((x_i - inc_y, y_i - inc_x))

                x_i += inc_x
                y_i += inc_y

        if save_to_memory:
            # Add viewed tiles to player's memory.
            self.tile_memory.update(visible_tiles)
        return visible_tiles

    def raw_attack_damage(self) -> int:
        """Returns the player's raw attack stat."""
        return self.attack
