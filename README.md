## Pyhack
#### Requirements
This game requires Python version 3.6 or later. <br>
The game should be run under a curses-compatible terminal emulator.

#### Running pyhack
To launch the game, execute `pyhack.py`.