"""
    Provides the Entity abstract class.
"""
from abc import ABC, abstractmethod
from typing import Tuple

from pyhack_state import GameState

Point = Tuple[int, int]


class Entity(ABC):
    """
        Abstract class that represents a game entity,
        which can be an enemy, an item on the floor, or
        the player.
    """

    def __init__(self, x: int, y: int):
        self.position: Point = (x, y)

    @abstractmethod
    def turn_update(self, game_state: GameState) -> bool:
        """
            Abstract method that should handle a game turn for this entity.
        """

    @abstractmethod
    def get_char_display(self) -> str:
        """
            Abstract method that should return the character
            that represents the entity on screen.
        """

    @abstractmethod
    def get_name(self) -> str:
        """
            Asbtract method that should return this entity's
            name (to be displayer to the user)
        """

    @abstractmethod
    def is_solid(self) -> bool:
        """
            Abstract method that should return True if
            this entity cannot be walked on.
        """

    @abstractmethod
    def is_hostile(self) -> bool:
        """
            Abstract method that should return True if
            this entity is an enemy.
        """

    @abstractmethod
    def attack_damage(self) -> int:
        """
            Abstract method that should return the amount
            of damage this entity deals if attacked.
        """
        return 0
