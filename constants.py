"""
    Contains constants used throughout Pyhack.
"""
import curses
from math import cos, sin, pi
from typing import List, Tuple

VERSION = "0.1"

# Player-related constants.
INITIAL_HP: int = 20
INITIAL_ATK: int = 3
HEALTH_REGEN_TURN_INTERVAL: int = 20

# Vision ray-casting constants.
VISION_RAY_COUNT: int = 360
VISION_RAY_SAMPLING_INTERVAL: float = 1
VISION_DELTA_THETA: float = (2 * pi) / VISION_RAY_COUNT
VISION_COS_SIN: List[Tuple[float, float]] = [
    (cos(n * VISION_DELTA_THETA), sin(n * VISION_DELTA_THETA))
    for n in range(VISION_RAY_COUNT)
]

# Dungeon level generation constants.
LEVEL_WIDTH: int = 100
LEVEL_HEIGHT: int = 100
LEVEL_ROOM_COUNT: int = 7

LEVEL_ENEMY_COUNT: int = 10

WELCOME_MESSAGE: str = f"Welcome to Pyhack v{VERSION} !"
INPUT_PROMPT_MESSAGE: str = ""
MESSAGE_HISTORY_WIDTH: int = 60
MESSAGE_HISTORY_HEIGHT: int = 10

DEFAULT_COLORS = [
    ('green', curses.COLOR_GREEN, curses.COLOR_BLACK),
    ('red', curses.COLOR_RED, curses.COLOR_BLACK),
    ('blue', curses.COLOR_BLUE, curses.COLOR_BLACK)
]
