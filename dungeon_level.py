"""
    Modules used for represnetation and storage
    of a dungeon level's layout.
"""
from enum import Enum
from typing import List, Tuple, Optional
from custom_types import Vec2i, Room

class DungeonTile(Enum):
    """Enumeration of dungeon tile values."""

    EMPTY = "."
    WALL = u"█"


def is_walkable(tile_type: DungeonTile) -> bool:
    """Returns True if the player can walk on the given tile type."""
    return tile_type != DungeonTile.WALL


class DungeonLevel:
    """Holds a dungeon level's layout."""

    def __init__(self, width: int, height: int) -> None:
        self.grid_data: List[List[DungeonTile]] = [
            [DungeonTile.WALL for y in range(height)] for x in range(width)
        ]
        self.width, self.height = width, height
        self.rooms: List[Room] = []

    def set_cell(self, c_x: int, c_y: int, val: DungeonTile) -> None:
        """Sets the cell value at the given position."""
        if not (0 <= c_x < self.get_width() and 0 <= c_y < self.get_height()):
            raise IndexError()
        self.grid_data[c_x][c_y] = val

    def get_cell(self, c_x: int, c_y: int) -> DungeonTile:
        """Returns the cell value at the given position."""
        if not (0 <= c_x < self.get_width() and 0 <= c_y < self.get_height()):
            raise IndexError()
        return self.grid_data[c_x][c_y]

    def add_room(self, room: Room):
        """Adds the given room to the level, clearing its insides"""
        r_x, r_y, r_w, r_h = room
        for i_x in range(r_x, r_x + r_w):
            for i_y in range(r_y, r_y + r_h):
                self.set_cell(i_x, i_y, DungeonTile.EMPTY)
        self.rooms.append(room)

    def find_room(self, position: Vec2i) -> Optional[Room]:
        """
            Returns this dungeon's room where the given position is located.
            If the position is invalid or the position doesn't match a room,
            returns None.
        """
        p_x, p_y = position
        for r_x, r_y, r_w, r_h in self.rooms:
            if r_x <= p_x < r_x + r_w and r_y <= p_y < r_y + r_h:
                return (r_x, r_y, r_w, r_h)
        return None

    def is_valid_position(self, *args) -> bool:
        """Checks whether the given position is within the level."""
        if len(args) == 2:
            return 0 <= args[0] < self.width and 0 <= args[1] < self.height

        if len(args) == 1 and isinstance(args[0], tuple):
            return self.is_valid_position(args[0][0], args[0][1])
        return False

    def get_width(self) -> int:
        """Returns the height of this dungeon level (in tiles)."""
        return len(self.grid_data)

    def get_height(self) -> int:
        """Returns the height of this dungeon level (in tiles)."""
        return len(self.grid_data[0])

    def get_size(self) -> Tuple[int, int]:
        """Returns the width and height of this level."""
        return (self.get_width(), self.get_height())

    def __str__(self) -> str:
        grid_str = ""
        for g_y in range(self.height):
            for g_x in range(self.width):
                grid_str += self.grid_data[g_x][g_y].value
            grid_str += "\n"

        return grid_str
