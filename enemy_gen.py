"""
    Handles generation and placement of enemies in a dungeon level.
"""
from random import randint
from typing import List

from dungeon_level import DungeonLevel, is_walkable
from entity import Entity
from pyhack_monster import Monster


def generate_enemies(dun_lev: DungeonLevel, num_enemies: int) -> List[Entity]:
    """
       Generates the given number of enemies for the given level,
       and returns a list conatining the entities representing said enemies.
    """
    entities: List[Entity] = []
    while len(entities) != num_enemies:
        r_x, r_y = (
            randint(0, dun_lev.get_width() - 1),
            randint(0, dun_lev.get_height() - 1),
        )
        if is_walkable(dun_lev.get_cell(r_x, r_y)):
            entities.append(Monster(r_x, r_y))

    return entities
