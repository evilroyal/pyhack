"""Provides the GameState data class."""
from random import randint

import constants

from dungeon_gen import generate_dungeon
from dungeon_level import DungeonTile
from pyhack_player import Player


# pylint: disable=R0903, #R0912, R0914
class GameState:
    """
        Class that represents the current state of a Pyhack game.
        Stores the current level, the player, etc...
    """
    def __init__(self):
        # Dungeon generation
        dun_w, dun_h, dun_r = (
            constants.LEVEL_WIDTH,
            constants.LEVEL_HEIGHT,
            constants.LEVEL_ROOM_COUNT,
        )
        self.current_level = generate_dungeon(dun_w, dun_h, dun_r)

        # Random player placement in empty space.
        player_x, player_y = randint(0, dun_w - 1), randint(0, dun_h - 1)
        while self.current_level.get_cell(player_x, player_y) != DungeonTile.EMPTY:
            player_x, player_y = randint(0, dun_w - 1), randint(0, dun_h - 1)

        self.player = Player((player_x, player_y))

        self.turn_number: int = 1
