"""
    Stores keyboard state globally for access by various modules.
"""
import curses
import sys

from typing import Set


class KeyInput:
    """
        Static class that allows access to keyboard input globally.
    """

    def __init__(self):
        print("KeyInput class was initialized ! This is a mistake.", file=sys.stderr)

    current_keys: Set[int] = set()

    @classmethod
    def update_inputs(cls, screen):
        """
            Updates internal input state using the given curses screen.
            Should be called at every game update.
        """
        cls.current_keys.clear()
        cur_key = screen.getch()
        while cur_key != curses.ERR:
            cls.current_keys.add(cur_key)
            cur_key = screen.getch()

    @classmethod
    def is_pressed(cls, key) -> bool:
        """Returns True iff the given curses key is pressed."""
        return key in cls.current_keys
