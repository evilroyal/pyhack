#!/bin/env python3
"""
    Main executable script for Pyhack.
"""
from sys import argv

from better_curses import wrapper
from pyhack_game import PyhackGame
from dungeon_gen import generate_dungeon


def curses_main(screen):
    """
        Main Curses function that starts a Pyhack game on the
        given Curses screen.
    """
    game = PyhackGame.get_instance(screen)

    game.start()

    # Ensure the destructor is called to reset terminal modifications.
    del game


def main():
    """Main function"""
    if len(argv) > 1:
        if argv[1] == "--generate-only":
            d_w, d_h = 100, 100
            if len(argv) > 2:
                d_w = int(argv[2])
            if len(argv) > 3:
                d_h = int(argv[3])
            d_r = generate_dungeon(d_w, d_h)
            print(d_r)
    else:
        wrapper(curses_main)


if __name__ == "__main__":
    main()
