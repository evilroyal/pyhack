"""
    Provides an interface to the curses modules.
    Encapsulates non-typed curses codes in this module.
"""
import curses
from typing import Callable, Dict

import constants
from custom_types import Vec2i

class CursesScreen:
    """
        Represents a curses-controlled terminal window.
    """

    def __init__(self, stdscr) -> None:
        self.stdscr = stdscr

    def no_delay(self, flag: bool) -> None:
        """Enables/disables no-delay mode for input"""
        self.stdscr.nodelay(flag)

    def get_size(self) -> Vec2i:
        """Returns the width and height of the terminal window (in characters)"""
        height, width = self.stdscr.getmaxyx()
        return (width, height)

    def draw_string(self, x_pos: int, y_pos: int, string: str, attr=None) -> bool:
        """
            Draws the given string to this screen, at a given position.
            If there isn't enough space on the screen to draw the string,
            it will not be drawn at all.
            Curses attributes can be given using the last argument.
            Returns True if the stringwas actually drawn.
        """
        max_y, max_x = self.stdscr.getmaxyx()

        if 0 <= x_pos + len(string) < max_x and 0 <= y_pos < max_y:
            if attr is None:
                self.stdscr.addstr(y_pos, x_pos, string)
            else:
                self.stdscr.addstr(y_pos, x_pos, string, attr)
            return True
        return False

    def erase(self) -> None:
        """Erases the whole screen, without refreshing immediately."""
        self.stdscr.erase()

    def border(self) -> None:
        """Draws a border around the screen."""
        self.stdscr.border()

    def refresh(self) -> None:
        """Redraws the whole screen."""
        self.stdscr.refresh()

    def getch(self) -> int:
        """
            Returns a currently pressed key's id,
            or curses.ERR if no keys are pressed.
        """
        return self.stdscr.getch()

class CursesColors:
    """Static class that procides curses colors."""
    __initialized = False
    __colors: Dict[str, int] = {}

    @classmethod
    def init(cls):
        """Initializes the colors to curses. SHould be called before using any color."""
        if cls.__initialized:
            return

        color_counter = 1
        for name, foreground, background in constants.DEFAULT_COLORS:
            curses.init_pair(color_counter, foreground, background)
            cls.__colors[name] = color_counter

            color_counter += 1

        cls.__initialized = True

    @classmethod
    def get(cls, clr_name: str) -> int:
        """Returns the curses attribute for a color given by name"""
        if clr_name in cls.__colors:
            return curses.color_pair(cls.__colors[clr_name])
        return 0

def __wrapper(stdscr, wrap_func: Callable[[CursesScreen], None]) -> None:
    wrap_func(CursesScreen(stdscr))

def wrapper(wrap_func: Callable[[CursesScreen], None]) -> None:
    """
        Initializes a curses screen and calls the given function with it.
        Ensures the terminal is returned to a correct state, even after an
        exception.
    """
    curses.wrapper(__wrapper, wrap_func)
