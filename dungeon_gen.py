"""
    Handles random generation of Pyhack dungeon levels.
"""
from math import floor, sqrt
from random import randint
from typing import Dict, List, Optional, Tuple

from dungeon_level import DungeonLevel, DungeonTile
from custom_types import Room

def rooms_intersect(room: Room, other_rooms: List[Room]) -> bool:
    """Return True if the given room overlaps with any of the given list"""
    nr_x, nr_y, nr_w, nr_h = room
    for o_x, o_y, o_w, o_h in other_rooms:
        for c_x in range(o_x, o_x + o_w):
            for c_y in range(o_y, o_y + o_h):
                if (nr_x - 1 <= c_x <= nr_x + nr_w + 1 \
                    and nr_y - 1 <= c_y <= nr_y + nr_h + 1):
                    return True

    return False

def generate_room(dun_w: int, dun_h: int, min_w: int, min_h: int) -> Room:
    """Generates a new room that fits in a level with given size"""
    nr_x, nr_y = (
        randint(1, dun_w - min_w - 1),
        randint(1, dun_h - min_h - 1),
    )
    nr_w, nr_h = (
        randint(min_w, dun_w - nr_x - 1),
        randint(min_h, dun_h - nr_y - 1),
    )

    return (nr_x, nr_y, nr_w, nr_h)

def generate_dungeon(
        width: int, height: int, room_count: Optional[int] = None
) -> DungeonLevel:
    """
        Randomly generates a dungeon's rooms and corridors of given
        height and width.
    """
    # If room count unspecified, generate it using magical formula.
    if room_count is None:
        room_count = floor(sqrt(width + height) / 2)

    # Creating set of random, non-intersecting rooms.
    min_width, min_height = width // 20, height // 20
    rooms: List[Room] = []
    while len(rooms) != room_count:
        room = generate_room(width, height, min_width, min_height)

        if rooms_intersect(room, rooms):
            continue

        rooms.append(room)

    dun_level = DungeonLevel(width, height)
    for room in rooms:
        dun_level.add_room(room)

    generate_paths(rooms, dun_level)

    return dun_level


def generate_paths(rooms: List[Room], dun_level: DungeonLevel) -> None:
    """
        Generates corridors in the given dungeon,
        considering the given set of rooms.
        Makes all rooms reachable.
    """
    # Graph intialization
    room_graph: Dict[int, List[int]] = {}
    for i, _ in enumerate(rooms):
        room_graph[i] = []

    # We add vertices to the graph at random until it's connected.
    paths: List[Tuple[int, int]] = []
    while not is_connected(len(rooms), room_graph):
        p_start, p_end = randint(0, len(rooms) - 1), randint(0, len(rooms) - 1)
        if p_start == p_end or (p_start, p_end) in paths or (p_end, p_start) in paths:
            continue

        paths.append((p_start, p_end))
        room_graph[p_start].append(p_end)
        room_graph[p_end].append(p_start)

    draw_hallways_from_graph(paths, dun_level, rooms)


def draw_hallways_from_graph(
        paths_graph: List[Tuple[int, int]], dun_level: DungeonLevel, rooms: List[Room]
):
    """Use the generated connected graph to create the hallways linking the rooms."""
    for start_ind, end_ind in paths_graph:
        start_room_x, start_room_y, start_room_w, start_room_h = rooms[start_ind]
        end_room_x, end_room_y, end_room_w, end_room_h = rooms[end_ind]

        start_x = randint(start_room_x, start_room_x + start_room_w - 1)
        start_y = randint(start_room_y, start_room_y + start_room_h - 1)

        end_x = randint(end_room_x, end_room_x + end_room_w - 1)
        end_y = randint(end_room_y, end_room_y + end_room_h - 1)

        current_x, current_y = start_x, start_y
        while not (current_x == end_x or current_y == end_y):
            if randint(0, 1):
                current_x += 1 if current_x < end_x else -1
            else:
                current_y += 1 if current_y < end_y else -1
            dun_level.set_cell(current_x, current_y, DungeonTile.EMPTY)

        if current_x == end_x:
            while current_y != end_y:
                current_y += 1 if current_y < end_y else -1
                dun_level.set_cell(current_x, current_y, DungeonTile.EMPTY)
        elif current_y == end_y:
            while current_x != end_x:
                current_x += 1 if current_x < end_x else -1
                dun_level.set_cell(current_x, current_y, DungeonTile.EMPTY)


def is_connected(room_count: int, room_graph: Dict[int, List[int]]) -> bool:
    """
        Returns True iff the room graph given as an adjacency table
        is connected, that is, the is a path between any two rooms.
    """
    visited: List[int] = []
    to_visit = [list(room_graph.keys())[0]]

    while to_visit != []:
        visiting = to_visit.pop(0)
        if visiting in visited:
            continue

        for neigh in room_graph[visiting]:
            if neigh not in visited:
                to_visit.append(neigh)
        visited.append(visiting)

    return len(visited) == room_count
