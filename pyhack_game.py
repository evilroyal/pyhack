"""
    Main module for the Pyhack game.
    Provides the PyhackGame singleton class that
    handles ececution of the game.
"""
import curses
import sys
from time import process_time, sleep
from typing import Optional, List
from threading import Timer

import constants
from better_curses import CursesScreen, CursesColors
from dungeon_level import is_walkable
from enemy_gen import generate_enemies
from keyboard_input import KeyInput
from pyhack_state import GameState
from entity import Entity
from pyhack_monster import Monster

class PyhackGame:
    """
        Singleton class that represents, and handles
        execution of a game of Pyhack.
    """

    game_instance: Optional["PyhackGame"] = None # Current instance of this class.
    tps: int = 75  # Number of game updates per second.

    @classmethod
    def get_instance(cls, screen) -> 'PyhackGame':
        """
            Returns the current instance of PyhackGame,
            and creates one if none exist.
        """
        if cls.game_instance is None:
            cls.game_instance = PyhackGame(screen)
        return cls.game_instance

    def __init__(self, screen: CursesScreen) -> None:
        # Curses setup.
        self.screen: CursesScreen = screen
        self.screen.no_delay(True)
        curses.raw(True)
        curses.curs_set(0)
        CursesColors.init()

        self.window_w, self.window_h = self.screen.get_size()

        self.game_state = GameState()

        self.entities = generate_enemies(
            self.game_state.current_level, constants.LEVEL_ENEMY_COUNT
        )

        # Information message system setup
        self.info_message = ""
        self.info_message_history: List[str] = []
        self.show_message_history = False
        self.message_reset_timer: Optional[Timer] = None
        self.show_info_message(constants.WELCOME_MESSAGE, 5000)

        self.redraw_requested = True

        self.running = False

    def __del__(self) -> None:
        # Cancel message reset timer, in order not to block exit.
        if self.message_reset_timer is not None:
            self.message_reset_timer.cancel()
        PyhackGame.game_instance = None
        curses.curs_set(1)

    def start(self) -> None:
        """Starts the execution of this Pyhack game."""
        self.running = True
        self.render()
        while self.running is True:
            start = process_time()
            self.update()
            self.render()
            end = process_time()

            sleep(max(0, 1 / self.tps - (end - start)))

    def update(self) -> None:
        """
            Updates the game logic and handles input.
            Returns True if the internal game state has changed and
            graphics should be updated.
        """
        should_redraw = False
        new_turn = False

        # Get pressed key(s).
        KeyInput.update_inputs(self.screen)

        if KeyInput.is_pressed(curses.KEY_END):
            self.__del__()
            sys.exit(0)

        # Handle player movement.
        player_x, player_y = self.game_state.player.position
        level_w, level_h = self.game_state.current_level.get_size()
        next_x, next_y = self.game_state.player.position
        if KeyInput.is_pressed(curses.KEY_UP):
            next_y = max(0, player_y - 1)
        if KeyInput.is_pressed(curses.KEY_RIGHT):
            next_x = min(level_w - 1, player_x + 1)
        if KeyInput.is_pressed(curses.KEY_DOWN):
            next_y = min(level_h - 1, player_y + 1)
        if KeyInput.is_pressed(curses.KEY_LEFT):
            next_x = max(0, player_x - 1)
        if KeyInput.is_pressed(ord("k")):
            self.show_message_history = not self.show_message_history
            self.redraw_requested = True

        can_walk_next = (
            next_x,
            next_y,
        ) != self.game_state.player.position and is_walkable(
            self.game_state.current_level.get_cell(next_x, next_y)
        )
        attack_target: Optional[Entity] = None
        for ent in self.entities:
            if ent.is_solid() and ent.position == (next_x, next_y):
                can_walk_next = False
                if ent.is_hostile():
                    attack_target = ent

        # Move player (and pass turn) only if destination tile is walkable.
        if can_walk_next:
            self.game_state.player.set_position(next_x, next_y)
            new_turn = True
        elif attack_target is not None and isinstance(attack_target, Monster):
            attack_target.current_hp -= self.game_state.player.raw_attack_damage()
            self.game_state.player.current_hp -= attack_target.attack_damage()

            new_turn = True

            if attack_target.current_hp <= 0:
                self.entities.remove(attack_target)
            self.show_info_message(
                f"{'Attacked' if attack_target.current_hp > 0 else 'Killed'} {attack_target.get_name()} - Dealt : { self.game_state.player.raw_attack_damage()} - Received : {attack_target.attack_damage()}",
                2500
            )

            if self.game_state.player.current_hp <= 0:
                sys.exit(1)

        # If the player has completed an action : process turn for other entities.
        if new_turn:
            for ent in self.entities:
                should_redraw = ent.turn_update(self.game_state) or should_redraw
            self.game_state.turn_number += 1
            if self.game_state.turn_number % constants.HEALTH_REGEN_TURN_INTERVAL == 0:
                self.game_state.player.current_hp = min(self.game_state.player.current_hp + 1, self.game_state.player.max_hp)

        self.redraw_requested = self.redraw_requested or should_redraw or new_turn

    def render(self) -> None:
        """Renders the current game state to the window."""
        if self.screen.get_size() != (self.window_w, self.window_h):
            self.window_w, self.window_h = self.screen.get_size()
            self.redraw_requested = True

        # Redraw only if the underlying game model has changed.
        if not self.redraw_requested:
            return

        # Clear screen, then render player's current viewport.
        self.screen.erase()

        player_x, player_y = self.game_state.player.position
        level_w, level_h = self.game_state.current_level.get_size()

        # Compute player's FOV.
        field_of_view = (
            player_x - self.window_w // 2,
            player_y - self.window_h // 2,
            player_x + self.window_w // 2,
            player_y + self.window_h // 2,
        )
        tiles_in_view = self.game_state.player.compute_field_of_view(
            self.game_state.current_level, field_of_view
        )

        # Draw the visible and memorized dungeon level to the screen.
        for win_y in range(1, self.window_h - 1):
            for win_x in range(1, self.window_w - 1):
                dungeon_x = (player_x - self.window_w // 2) + win_x
                dungeon_y = (player_y - self.window_h // 2) + win_y
                if not (0 <= dungeon_x < level_w and 0 <= dungeon_y < level_h):
                    self.screen.draw_string(win_x, win_y, "")
                    continue

                # Visible tile : draw it bright.
                if (dungeon_x, dungeon_y) in tiles_in_view:
                    self.screen.draw_string(
                        win_x,
                        win_y,
                        self.game_state.current_level.get_cell(
                            dungeon_x, dungeon_y
                        ).value,
                    )
                elif (  # Previously seen tile : draw it dim.
                        dungeon_x,
                        dungeon_y,
                ) in self.game_state.player.tile_memory:
                    self.screen.draw_string(
                        win_x,
                        win_y,
                        self.game_state.current_level.get_cell(
                            dungeon_x, dungeon_y
                        ).value,
                        curses.A_DIM,
                    )

        # Player : draw green '@' symbol.
        self.screen.draw_string(
            self.window_w // 2, self.window_h // 2, "@", curses.A_BOLD | CursesColors.get('green')
        )

        for ent in self.entities:
            e_x, e_y = ent.position
            if ent.position in tiles_in_view:
                self.screen.draw_string(
                    e_x + (self.window_w // 2 - player_x),
                    e_y + (self.window_h // 2 - player_y),
                    ent.get_char_display(),
                    curses.A_BOLD | CursesColors.get('red')
                )

        # Draw screen border.
        self.screen.border()

        # Draw info HUD over border.
        self.draw_info_bar()

        self.screen.refresh()
        self.redraw_requested = False

    def reset_info_message(self) -> None:
        """Removes the currentlyshown information message."""
        self.info_message_history.insert(0, self.info_message)
        self.info_message = constants.INPUT_PROMPT_MESSAGE
        self.redraw_requested = True


    def show_info_message(self, message: str, timeout: int = 0) -> None:
        """
            Shows the given message in the information bar.
            If timeout is higher that zero, the message will disappear after the given
            number of milliseconds.
        """
        if self.info_message != "":
            self.info_message_history.insert(0, self.info_message)
        self.info_message = message
        self.redraw_requested = True

        if timeout > 0:
            if self.message_reset_timer is not None:
                self.message_reset_timer.cancel()
            self.message_reset_timer = Timer(timeout/1000, self.reset_info_message)
            self.message_reset_timer.start()

    def draw_info_bar(self) -> None:
        """
            Draws the info bar at the bottom of the screen.
        """
        self.screen.draw_string(
            2,
            self.window_h - 1,
            f"Turn {self.game_state.turn_number} -- HP: {self.game_state.player.current_hp}/{self.game_state.player.max_hp}"
        )

        self.screen.draw_string(
            self.window_w - 2 - len(self.info_message),
            self.window_h - 1,
            self.info_message
        )

        if self.show_message_history:
            for y_i in range(self.window_h - constants.MESSAGE_HISTORY_HEIGHT, self.window_h - 1):
                self.screen.draw_string(self.window_w - constants.MESSAGE_HISTORY_WIDTH - 1, y_i, " " * constants.MESSAGE_HISTORY_WIDTH)
            self.screen.draw_string(self.window_w - 1 - constants.MESSAGE_HISTORY_WIDTH, self.window_h - 1 - constants.MESSAGE_HISTORY_HEIGHT, "┌" + "─" * (constants.MESSAGE_HISTORY_WIDTH - 1))
            self.screen.draw_string(self.window_w - constants.MESSAGE_HISTORY_WIDTH + 1, self.window_h - 1 - constants.MESSAGE_HISTORY_HEIGHT, "Event History")
            for y_i in range(self.window_h - constants.MESSAGE_HISTORY_HEIGHT, self.window_h - 1):
                self.screen.draw_string(self.window_w - 1 - constants.MESSAGE_HISTORY_WIDTH, y_i, "│")

            for i in range(0, min(constants.MESSAGE_HISTORY_HEIGHT - 2, len(self.info_message_history))):
                self.screen.draw_string(self.window_w - constants.MESSAGE_HISTORY_WIDTH + 1, self.window_h - constants.MESSAGE_HISTORY_HEIGHT + i, self.info_message_history[i])

            if len(self.info_message_history) > constants.MESSAGE_HISTORY_HEIGHT - 1:
                self.info_message_history = self.info_message_history[0:constants.MESSAGE_HISTORY_HEIGHT]
